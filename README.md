# copy-tuflowfv-model

Copies a model or list of models and all required input files to a user defined output directory. 

## Description
In the python script copy_fvc.py you provide two inputs:
	A csv input file with a single TUFLOW FV Control File (FVC) on each row. 
	A location to save the models
The script will then copy all required control and input files. Ideal for packaging models for data provision or archiving. 

## Contributing
Please feel free to add new features. Please branch from main, add your features and once happy submit a merge request. 

## Authors and acknowledgment
Thanks to Toby Devlin and Pamela Wong for their contributions.

## License
MIT License.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
