#!/usr/local/bin/python3.7
# -*- coding: utf-8 -*-
"""
Updated from the old version of things in 2019
Should handle a few new features in TFV, and fix up some old functionality. Can now flatten directories and make a
list of files to deploy. In future will add features to zip up all required files/folders into a package to deploy
@author : toby.devlin
"""

import pathlib
from os import getcwd
from os import name as platform_name
from os import system
from os import sep as pathsep
from shutil import copyfile
from email.mime.text import MIMEText
import smtplib
from socket import gethostname

# Initial global Variables
host_name = gethostname()
if platform_name == 'nt':
    platform = 'WINDOWS'
elif platform_name == 'posix':
    platform = 'LINUX'
else:
    print('ERROR: May not be working on platforms other than Linux or windows')
    exit(1)


class FileItem:
    """
    Abstraction layer over pathlib. Uses composition to get a relative path and parent of it if applicable, a full path
    and a full file if applicable.
    Mainly used to keep track of the relative path required, but also the root that it is relative to.
    Otherwise all attributes of this are pathlib objects
    """

    def __init__(self, path, parent=None):
        # Parse Main Path
        self.new_fil = None  # Default for later copy stuff
        if isinstance(path, str):
            self.path = pathlib.Path('/'.join(path.split('\\')))
        elif isinstance(path, FileItem):
            self.path = path.path
        elif issubclass(path.__class__, pathlib.Path):  # Either a WindowsPath or UnixPath
            self.path = path

        if self.path.is_absolute():
            self.is_abs = True
            self.parent = None
        else:
            self.is_abs = False
            if isinstance(parent,
                          FileItem):  # Probably could be the same as the 'else' but this way tracks 'parent parent'
                self.parent = parent
            elif isinstance(parent, str):
                self.parent = FileItem(parent.replace('\\', pathsep), None)
            else:
                self.parent = FileItem(parent, None)
        self.is_dir = self.fullfile().is_dir()

    def fullfile(self):  # Shouldn't really be used for directories... will work anyway
        if self.is_abs:
            return self.path
        else:
            return self.parent.fullpath().joinpath(self.path)  # Should work recursively to solve full path

    def fullpath(self):
        if self.is_dir:
            return self.fullfile()
        else:
            return self.fullfile().parent

    def exists(self):
        return self.fullfile().exists()

    def get_relative_to(self, new_root):
        """
        Returns a FileItem of the same file as the base one, but relative to the new_root
        :param new_root: A new place for the file to be relative to
        :return: FileItem
        """
        try:
            com_pat = self.fullfile().relative_to(new_root.fullpath())
            return com_pat
        except ValueError:
            return None
        # return FileItem(com_pat, new_root)

    def copy_over(self, new_stem=None, old_stem=None):
        """
        Function to copy this file to a new location. New location will be created using the new_name method. If the
        new_name method has already been run then this will use that.
        :return: A FileItem of the file in the new location
        """
        if self.new_fil is None:
            if new_stem is not None:
                self.new_name(new_stem, old_stem)
            if self.new_fil is None:
                return

        # Check if folder and stop here if so
        if self.is_dir:
            self.new_fil.fullfile().mkdir(parents=True, exist_ok=True)
        else:
            self.new_fil.fullpath().mkdir(parents=True, exist_ok=True)
            if not self.fullfile().resolve().exists():
                print('File does not exist yet: ' + str(self.fullfile().resolve()))
            else:
                copyfile(str(self.fullfile().resolve()), str(self.new_fil.fullfile().resolve()))

    def new_name(self, new_stem, old_stem=None):
        """
        Creates a sub object with a new file location. Replaces the 'old_stem' with the 'new_stem'. If 'old_stem' is
        None, then it copies just the file into new_stem without any other relative link.
        Can be used to get a new name for a file without copying it yet. But copy it later if required.
        :param old_stem: The stem that this file is relative to that should be changed
        :param new_stem: The new stem that will replace the above.
        :return:
        """

        # Setup new file, depending on mode
        if old_stem is None:
            self.new_fil = FileItem(self.fullfile().name, new_stem)
        else:
            rel_fil = self.get_relative_to(old_stem)
            if rel_fil is not None:
                self.new_fil = FileItem(rel_fil, new_stem)
        return self.new_fil


class ModelManifest:

    def __init__(self, fvcfile, run_dir=None):
        """
        Main TUFLOWFV Model manifest object. Reads an fvc file and creates a list of all the files required and all the
        output files that will be created.
        TODO: Allow to change output directory, or flatten to 'inputs' and 'outputs'
        TODO: Allow to modify file names to strip out silly characters that aren't posix friendly.
        :param fvcfile: The main fvc of this model run
        """
        # Initialise Class Attributes
        self.File_List = list()
        self.Output_Files = list()
        self.working_dir = None  # the path of the control file
        self.current_parent = None  # Current root for relatives paths
        self.logdir = None
        self.turbdir = None
        self.wqdir = None
        self.outdir = None
        self.control_file = None
        self.back_manifest = None
        self.__legacy_include = False
        self.__includes_aed2 = False
        self.__includes_gotm = False
        self.__echogeo_nc = False
        self.__echogeo_csv = False

        # Initialise Control File
        self.set_control_file(fvcfile, run_dir)
        self.name = self.control_file.fullfile().stem

        # Loop through file and get all possible linked files
        self.parse_lines()

        # Prepare Output Types

    def set_control_file(self, control_fil, run_dir):
        """
        Sets the control file of the model manifest. This is the main file which should be read to get all further
        files.
        :param control_fil: the control file
        :param run_dir: the directory of the batch file, if none then will take the cwd
        :return:
        """
        if run_dir is None:
            self.control_file = FileItem(control_fil, pathlib.Path(getcwd()))
        else:
            self.control_file = FileItem(control_fil, pathlib.Path(run_dir))

        if not self.control_file.exists():
            print('ERROR: The control file does not exist!')
            exit()

        # Set path stuff
        self.working_dir = FileItem(self.control_file.fullpath(), None)
        self.current_parent = self.working_dir

        # Overwrite default directories
        self.wqdir = self.working_dir
        self.turbdir = self.working_dir
        self.outdir = self.working_dir
        self.logdir = FileItem('log', self.working_dir)
        if not self.logdir.exists():
            self.logdir = self.working_dir

    def parse_lines(self, fil=None):
        """
        Reads lines from a TUFLOWFV control file (or include type file) and parses the commands to extract any files
        that might be referenced by the model. Checks some other commands that have an effect on how files are treated.
        :return: None
        """
        if fil is None:  # Set default input file to the control file
            fil = self.control_file

        with open(fil.fullfile()) as f:
            for line in f:
                # Remove comments, things without arguments and empty lines
                if line.strip().startswith('!') or '==' not in line or line.strip() == "":
                    continue
                line = line.split('!')[0].strip()

                # Split on the '==' and clean up command
                token, remain = line.split('==')
                token = "".join(token.lower().split())

                if token in self.mapping:  # The easy one
                    self.mapping[token](self, remain)

                elif token == 'output':  # If its an output, try to get the name of it
                    self.__parse_output(remain, f)

    def add_file(self, fil):
        """
        Adds a file to the manifest list. Note that this does not guarantee that the file exists as yet.
        :param fil: The file (potentially relative)
        :return: A pathlib object of the file.
        """
        file_tmp = FileItem(fil, self.current_parent)
        self.File_List.append(file_tmp)
        return file_tmp

    def add_nmls(self):

        if self.__includes_gotm:
            self.add_file(str(self.turbdir.path) + '/gotmturb.nml')

        if self.__includes_aed2:
            self.add_file(str(self.wqdir.path) + '/aed2.nml')
            self.add_file(str(self.wqdir.path) + '/aed2_pathogen_pars.nml')
            self.add_file(str(self.wqdir.path) + '/aed2_phyto_pars.nml')
            self.add_file(str(self.wqdir.path) + '/aed2_zoop_pars.nml')

    def additional_outputs(self):
        # log and restarts
        self.add_output(self.logdir, '.log')
        self.add_output(self.logdir, '.rst')
        self.add_output(self.logdir, '_turb.rst')
        self.add_output(self.logdir, '_bed.rst')
        self.add_output(self.logdir, '_ptm.rst')
        self.add_output(self.logdir, '_ext_cfl_dt.csv')
        self.add_output(self.logdir, '_int_cfl_dt.csv')

        # Geo files
        if self.__echogeo_nc:
            self.add_output(self.logdir, '_geo.nc')
        if self.__echogeo_csv:
            self.add_output(self.logdir, '_geo.csv')
            self.add_output(self.logdir, '_geo_cellupdates.csv')
            self.add_output(self.logdir, '_geo_nodestring.csv')
            self.add_output(self.logdir, '_geo_out.csv')
            self.add_output(self.logdir, '_geo_struct.csv')

        # TODO: Add Check files here?

    def add_output(self, path, suffix):
        name = self.name + suffix
        outfil = FileItem(path.path.joinpath(name), path.parent)
        self.Output_Files.append(outfil)

    def deploy_files(self, remote):
        """
        Routine to zip files locally and deploy to a remote server
        :param remote: The new 'root' location for the fvc, with relative paths branching off that
        :return:
        """
        pass
        # TODO: write this.

    def copy_files(self, new_path):
        self.copy_controls(new_path)
        self.additional_outputs()
        self.add_nmls()
        self.copy_manifest(new_path)
        #self.make_back_manifest(new_path)

    def copy_manifest(self, new_path):
        """
        Routine to copy the manifest to a new location with constant relative paths. The new_path, is the new path to
        the main .fvc, which all relative files will (ultimately) be relative to.
        Any non-relative paths will be ignored by default.
        :param new_path: The new 'root' location for the fvc, with relative paths branching off that
        :return:
        """
        new_path = pathlib.Path(new_path)
        for fileI in self.File_List:
            fileI.copy_over(new_path, self.working_dir)

    def copy_controls(self, new_path):
        """
        Routine to copy the control file over and to make sure relevant folders exist. Can be used to not copy any
        larger BC files and the like.
        :param new_path: The new 'root' location for the fvc, with relative paths branching off that
        :return:
        """
        new_path = pathlib.Path(new_path)
        # Get the fvc over
        self.control_file.copy_over(new_path, self.working_dir)

        # Make Important Folders
        self.logdir.copy_over(new_path, self.working_dir)
        self.wqdir.copy_over(new_path, self.working_dir)
        self.turbdir.copy_over(new_path, self.working_dir)
        self.outdir.copy_over(new_path, self.working_dir)

    def make_back_manifest(self, new_path):
        """
        Writes a file with the list of files to copy back and where to copy them to. Put this file in the new path
        :param new_path: the new directory that files are relative to and also where to write the back manifest to.
        :return:
        """
        new_path = pathlib.Path(new_path)
        name = self.name + '_CopyBack.manifest'
        self.back_manifest = new_path.joinpath(name)
        with open(self.back_manifest, 'w') as f:
            logfil = str(self.logdir.new_fil.fullfile().resolve()) + pathsep + self.name + ".log"
            f.write(f'{logfil}\n')

            for fil in self.Output_Files:
                fil.new_name(new_path, self.working_dir)
                if fil.new_fil is not None:
                    f.write('{0} | {1}\n'.format(str(fil.new_fil.fullfile().resolve()), str(fil.fullfile().resolve())))

    def __parse_output(self, remain, f):
        closed = False
        params = None
        remain = remain.lower().strip()
        if remain in self.output_suffixes:  # Standard output suffixes
            suff = self.output_suffixes[remain]
        else:
            suff = ""

        # Loop through output block to find out what the output name will be
        while not closed:
            line = f.readline()
            line = line.split('!')[0]
            line = "".join(line.split())
            parts = line.split("==")
            token = "".join(parts[0].lower().split())
            if token.startswith('suffix'):
                suff = line.split('==')[-1].strip() + suff
                closed = True
            elif token.startswith('outputparameter') and '#' in suff:
                params = parts[-1].split(',')
            elif token.startswith('endoutput'):
                closed = True
            elif token in self.mapping:
                self.mapping[token](self, parts[1])

        if '#' in suff:  # Special stuff for dat files with variable in name
            for param in params:
                suff_tmp = suff.replace('#', param)
                self.add_output(self.outdir, suff_tmp)
            self.add_output(self.outdir, '.ALL.sup')
        elif 'xmdf' in suff:
            self.add_output(self.outdir, suff)
            self.add_output(self.outdir, '.xmdf.sup')
        else:
            self.add_output(self.outdir, suff)

    def __parse_standard(self, remain):
        """
        Standard commands in this sense are those where the only thing after the '==' is a file name. An example is
        restart file == ''. These can be easily added to the manifest.
        :param remain: the part of the fvc command after the '=='
        :return: None
        """
        fil = remain.split(',')[0].strip()
        self.add_file(fil)

    def __parse_gis(self, remain):
        """
        GIS lines are a bit different. They can have multiple files separated by '|', and usually require additional
        files that aren't explicitly referenced, i.e. mif files need mid files. Shape files need heaps of files.
        All of these need adding to the manifest.
        TODO: make this robust to handling upper case and lower case file extensions, i.e. 'MIF' and 'SHP'
        :param remain: the part of the fvc command after the '=='
        :return: None
        """
        gis_list = remain.split('|')
        for gisfile in gis_list:
            gisfile = gisfile.strip()
            self.add_file(gisfile)
            if gisfile.endswith('.shp'):
                self.add_file(gisfile.replace(".shp", ".shx"))
                self.add_file(gisfile.replace(".shp", ".dbf"))
                self.add_file(gisfile.replace(".shp", ".prj"))
            elif gisfile.endswith('mif'):
                self.add_file(gisfile.replace(".mif", ".mid"))
            else:
                print('Unknown GIS file type: ' + gisfile)
                print('Make sure GIS files end with ''shp'' or ''mif''. This is case sensitive')  # TODO: make not so...

    def __parse_include(self, remain):
        """
        Relative paths in include files are treated as though relative to the include file unless the 'legacy include
        paths' command is set to 1 (true). It needs adding to the manifest, but also needs parsing to read in any
        commands that might be within it.
        :param remain: the part of the fvc command after the '=='
        :return: None
        """
        inc_fil = self.add_file(remain.strip())

        # Handle relative path switch if required
        old_parent = self.current_parent
        if not self.__legacy_include:
            self.current_parent = inc_fil.fullpath()

        # Now do recursive thing
        self.parse_lines(inc_fil)

        # Switch back if required.
        self.current_parent = old_parent

    def __parse_sed(self, remain):
        """
        Handles the sediment control file. The only file that this usually references is the bed restart. But it treats
        everything as relative to the main working directory regardless of the include path settings. As if legacy
        include paths is set on for sediment control files. This may (should?) change in future releases of FV, so is
        here as a separate function just in case.
        :param remain: the part of the fvc command after the '=='
        :return: None
        """
        old_leg = self.__legacy_include
        self.__legacy_include = True
        self.__parse_include(remain)
        self.__legacy_include = old_leg

    def __parse_ptm(self, remain):
        """
        A function to parse the ptm control file. This should be treated as a straightforward include file for now. As
        more advanced features get added to the particle tracking module, it may become necessary to extend this.
        :param remain: the part of the fvc command after the '=='
        :return: None
        """
        # Place holder to allow for future extensions to this as required, same as include at the moment
        self.__parse_include(remain)

    def __parse_log(self, remain):
        """
        Keep track of the log directory. By default this is the working directory unless a folder exists in there called
        'log', in which case it is that folder. Can be manually set though. This folder is used for writing the log
        file, the restart files, the geo files, cfl_dt files, etc.
        :param remain: the part of the fvc command after the '=='
        :return: None
        """
        self.logdir = FileItem(remain.strip(), self.current_parent)

    def __parse_wq(self, remain):
        """
        Special case to keep track of the directory that the file aed2 nml files will be in. The files dont change name
        so different versions require different folders. The files will need adding to the manifest later potentially.
        :param remain: the part of the fvc command after the '=='
        :return: None
        """
        self.wqdir = FileItem(remain.strip(), self.current_parent)

    def __parse_turb(self, remain):
        """
        Special case to keep track of the directory that the file 'gotmturb.nml' will be in if required. As the nml file
        cannot change name, it is often put in different folders if different versions are required, or defaults to the
        main working directory.
        :param remain: the part of the fvc command after the '=='
        :return: None
        """
        self.turbdir = FileItem(remain.strip(), self.current_parent)

    def __parse_outputdir(self, remain):
        """
        Special case for keeping track of the output path for lines that begin 'output dir == '. Sets this for later use
        when putting together a list of files to copy back. Also used to make sure output directory exists.
        :param remain: the part of the fvc command after the '=='
        :return: None
        """
        self.outdir = FileItem(remain.strip(), self.current_parent)

    def __parse_swan(self, remain):
        # TODO: add stuff to handle swan parsing...
        pass

    def __parse_bc(self, remain):
        """
        Parses a 'BC == ' line and gets any files from it that might be referenced. Adds these files to the manifest
        :param remain: the part of the fvc command after the '=='
        :return: None
        """
        parts = remain.split(',')
        if parts[0].strip().upper() in ('ZG', 'QN'):
            return  # Types of boundaries with no files associated
        self.add_file(parts[-1].strip())

    def __parse_eg_all(self, remain):
        val = remain.strip().lower() == '1'
        self.___csv = val
        self.__echogeo_nc = val

    def __parse_eg_nc(self, remain):
        val = remain.strip().lower() == '1'
        self.__echogeo_nc = val

    def __parse_eg_csv(self, remain):
        val = remain.strip().lower() == '1'
        self.__echogeo_csv = val

    def __set_legacy_include(self, remain):
        """
        Determines from the legacy include paths line whether to assume relative paths in include files are relative to
        that file, or relative to the main fvc. Will be used later in this object for setting paths.
        :param remain: The part of the fvc command after the '=='
        :return: None
        """
        self.legacy_include = remain.strip() == "1"

    def __set_turb(self, remain):
        if 'external' in remain.lower():
            self.__includes_gotm = True

    def __set_wq(self, remain):
        if 'external' in remain.lower():
            self.__includes_aed2 = True

    def __parse_emptydir(self, remain):
        # TODO: Add empty files directory creation
        pass

    def __parse_checkdir(self, remain):
        # TODO: Add check file directory creation
        pass

    mapping = {
        # Special settings
        'legacyincludepaths': __set_legacy_include,
        'verticalmixingmodel': __set_turb,
        'waterqualitymodel': __set_wq,
        
        # SETUP
        'include': __parse_include,
        'sedimentcontrolfile': __parse_sed,
        'particletrackingcontrolfile': __parse_ptm,  # TODO: Keep an eye on this in case more work is needed
        'restartfile': __parse_standard,
        'restart': __parse_standard,
        'outputpointsfile': __parse_standard,
        'initialcondition2d': __parse_standard,
        'initialcondition3d': __parse_standard,
        'externalturbulencemodeldirectory': __parse_turb,
        'externalturbulencemodeldir': __parse_turb,
        'externalwaterqualitymodeldir': __parse_wq,
        'externalwaterqualitymodeldirectory': __parse_wq,
        'initialscalarprofile': __parse_standard,

        # Structures and bcs
        'bc': __parse_bc,
        'polygonfile': __parse_standard,
        'verticaldistributionfile': __parse_standard,
        'verticaldistribution': __parse_standard,
        'verticaldistributiona': __parse_standard,
        'verticaldistributionb': __parse_standard,
        'fluxfile': __parse_standard,
        'controlfile': __parse_standard,
        'culvertfile': __parse_standard,
        'targetfile': __parse_standard,
        'samplefile': __parse_standard,
        'scalarfile': __parse_standard,
        'widthfile': __parse_standard,
        'energylossfile': __parse_standard,
        'blockagefile': __parse_standard,
        'volumelimitfile': __parse_standard,
        'timeseriesfile': __parse_standard,

        # Geo
        'nodestringpolyline': __parse_standard,
        'nodestringpolylinefile': __parse_standard,
        'materialpolygonfile': __parse_standard,
        'materialpolygon': __parse_standard,
        'partitionfile': __parse_standard,
        'layerfacefile': __parse_standard,
        'layerfaces': __parse_standard,
        'geometry2d': __parse_standard,
        'cellelevationfile': __parse_standard,
        'cellelevationpolyline': __parse_standard,
        'cellelevationpolylinefile': __parse_standard,
        'cellelevationpolygon': __parse_standard,
        'cellelevationpolygonfile': __parse_standard,
        'cellelevationpoints': __parse_standard,
        'cellelevationtin': __parse_standard,
        'griddefinitionfile': __parse_standard,
        'echogeometry': __parse_eg_all,
        'echogeometrynetcdf': __parse_eg_nc,
        'echogeometrycsv': __parse_eg_csv,

        # GIS
        'shpprojection': __parse_standard,
        'readgiszline': __parse_gis,
        'readgismat': __parse_gis,
        'readgiszpts': __parse_gis,
        'readgridzpts':__parse_standard,
        'readgisnodestring': __parse_gis,
        'writeemptygisfiles': __parse_emptydir,
        'writecheckfiles': __parse_checkdir,

        # OUTPUTS
        'outputdir': __parse_outputdir,
        'logdir': __parse_log,
    }

    output_suffixes = {
        'netcdf': '.nc',
        'netcdfv': '.nc',
        'transport': '_trans.nc',
        'datv': '_#.dat',
        'dat': '_#.dat',
        'flux': '_FLUX.csv',
        'structflux': '_STRUCTFLUX.csv',
        'curtain': '_CURT.nc',
        'xmdf': '.xmdf',
        'points': '_POINTS.csv',
        'mass': '_MASS.csv',
        'structcheck': '_STRUCTCHECK.csv'
    }


if __name__ == "__main__":
    from sys import argv
    import os
    for argin in argv:
        #print(argin)
        if argin.endswith('.fvc'):
            #print(argin)
            #print(os.getcwd())
            fvc = ModelManifest(argin, os.getcwd() + '/')
        elif argin.endswith('/') or argin.endswith('\\'):
            #print(argin)
            new_dir = FileItem(argin)
            new_dir.is_dir = True
    #print(new_dir.fullpath())
    #print(new_dir.path)
    #print(new_dir.fullfile())
    #print(new_dir.is_dir)
    fvc.copy_files(new_dir.fullpath())
    
