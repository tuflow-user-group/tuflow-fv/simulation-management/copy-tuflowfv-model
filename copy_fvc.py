#!/usr/local/bin/python3.7
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 23:08:24 2020
Define csv with list of fvcs to read in
Define the out/copy folder
Using copy_fvc to copy all fvc listed in the csv to the respective folders
@author: pamela.wong
"""
import os
import pandas as pd
from fvutil import ModelManifest
from pathlib import Path

# ======= User Define Input ===================================================
copy_dir = r'C:\tmp'
csv_file = 'fvcs_for_copy.csv'
# =============================================================================

# ======= Let It Be ===========================================================
def splitall(path):
    allparts = []
    while 1:
        parts = os.path.split(path)
        if parts[0] == path:  # sentinel for absolute paths
            allparts.insert(0, parts[0])
            break
        elif parts[1] == path: # sentinel for relative paths
            allparts.insert(0, parts[1])
            break
        else:
            path = parts[0]
            allparts.insert(0, parts[1])
    return allparts

colnames=['FVC'] 
df = pd.read_csv(csv_file, names=colnames, header=None)

for ind in df.index:
    print (df['FVC'][ind])
    fil = (df['FVC'][ind])

    olddir, oldfil = os.path.split(fil)
    all_fold = splitall(olddir)
    folder = all_fold[4]
    
    dir = os.path.join(copy_dir, folder)
    if not os.path.exists(dir):
        os.mkdir(dir)
    
    copy_folder = os.path.join(dir, 'input/dummy/') 
    
    fvc = ModelManifest(oldfil, olddir)
    fvc.copy_files(copy_folder)

